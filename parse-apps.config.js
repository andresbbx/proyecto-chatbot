module.exports = {
    apps : [{
      name        : "Nodo 1",
      script      : "./bin/www.js",
      watch       : true,
      merge_logs  : true,
      cwd         : "/var/www/parse-apps/app1/",
     },{
      name        : "Nodo 2",
      script      : "./bin/www.js",
      watch       : true,
      merge_logs  : true,
      cwd         : "/var/www/parse-apps/app2/",
    }]
  }