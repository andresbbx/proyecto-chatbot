'use strict'

const env = process.env.NODE_ENV || 'development'
const config = require('../config/config')[env]
const assistantV1 = require('watson-developer-cloud/assistant/v1')

const ibmAssistant = new assistantV1({

    iam_apikey: config.ibm.iam_apikey,
    url: config.ibm.url,
    version: config.ibm.version
})




function configOptions(message,context) {
    if(message===null || context===null){
        throw "message or context in null"
    }
    let options = {
        workspace_id: config.ibm.workspaceid,
        headers: {
            'Custom-Header': 'custom',
            'Accept-Language': 'custom'

        },
        input: {
            text: message
        },
        context: context

    }

    return options

}






async function sendMessage(message, context) {
    
    try{
        let options = configOptions(message,context)
   
    
        return new Promise((resolve, reject) => {
            ibmAssistant.message(options, (err, response) => {
                
                if (err) {
                    
                    reject(err)
                }
                else {
                    
                    resolve(response)
    
                }
    
            })
    
        })

    }catch(e){
        throw new Error(e)

    }
 

    

    


}






module.exports = {

    sendMessage
}