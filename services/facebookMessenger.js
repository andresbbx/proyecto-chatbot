'use strict'
const request = require('request')
const env = process.env.NODE_ENV || 'development'
const config = require('../config/config')[env]
const facebookProfileService = require('../services/facebookInfoProfile')
const petitionApi = require('../util/petitionOfApi')
const componentFacebook = require('../util/componentFacebook')


const token = process.env.TOKEN_FACEBOOK_MESSENGER
 const uri = "messages"


async function sendingActionTyping(destinatario_id){

    let body = {
        "recipient":{
            "id":destinatario_id
        },
        "sender_action":"typing_on"
    }

    return petitionApi.sendPetitionApiToFacebook(uri,body)
}




async function sendCheckMessageToFacebook(destinatario_id){
    validateData("mensaje prueba para validar",destinatario_id)

    let body = {
        "recipient":{
            "id":destinatario_id
        },
        "sender_action":"mark_seen"
    }

    return petitionApi.sendPetitionApiToFacebook(uri,body)

}
async function sendTextToFacebook(messageSend, destinatario_id) {
  
    validateData(messageSend,destinatario_id)
    
   const resultadoTyping = await sendingActionTyping(destinatario_id)


        let body = {

            "messaging_type":'RESPONSE',
            "recipient": {
                "id": destinatario_id
            },
            "message": {
                "text": messageSend
            }
        }

        return petitionApi.sendPetitionApiToFacebook(uri,body)
   
}

async function sendTextGreetingToFacebook(messageSend, destinatario_id) {
    
    validateData(messageSend,destinatario_id)
    let profileFacebook = await facebookProfileService.getInfoProfile(destinatario_id)

    let saludate = messageSend.replace('{{nombre}}', profileFacebook.name)
    return sendTextToFacebook(saludate, destinatario_id)
}


async function sendButtomCallToFacebook(destinatario_id) {
    validateData("mensaje para validar",destinatario_id)
    const componentServices = await componentFacebook('buttomCall')
    let body = {
        "recipient": {
            "id": destinatario_id
        },

        componentServices
    }

    return petitionApi.sendPetitionApiToFacebook(uri,body)
}


async function sendQuestionQuickToFacebook(destinatario_id) {
    validateData("mensaje para validar",destinatario_id)
    const componentServices = await componentFacebook('questionQuick')
    let body = {
        "messaging_type": "RESPONSE",
        "recipient": {
            "id": destinatario_id
        },

        componentServices
    }

    return petitionApi.sendPetitionApiToFacebook(uri,body)
}


async function sendListServicesToFacebook(destinatario_id) {
    validateData("lista servicios",destinatario_id)
    let componentServices = await componentFacebook('listServices')
    let body = {
        "recipient": {
            "id": destinatario_id
        },
            componentServices
    }

    return petitionApi.sendPetitionApiToFacebook(uri,body)

}

function validateData(messageSend,destinatario_id){
    if (messageSend === null || destinatario_id === null ||messageSend==="" || destinatario_id==="") {
        throw new Error('message to send or destinatary id not null allowed')
    }
}


module.exports = {
    sendTextToFacebook,
    sendButtomCallToFacebook,
    sendTextGreetingToFacebook,
    sendListServicesToFacebook,
    sendCheckMessageToFacebook,
    sendQuestionQuickToFacebook,
    token
}