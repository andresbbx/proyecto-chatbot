'use strict'
const env = process.env.NODE_env || 'development'
const config = require('../config/config')[env]
const clienteTwilio = require('twilio')(config.twilio.accountSid, config.twilio.authToken)


async function sendMessageToWhatsApp(information) {

    const result = await clienteTwilio.messages.create({

        to: information.To,
        from: information.From,
        body: information.message



    })

    return result



}



async function sendGreetingToWhatsApp(information) {
    let numberDestinatario = information.To.split('+')
    let messageSaludate = information.message.replace('{{nombre}}', numberDestinatario[1] )
    const result = await clienteTwilio.messages.create({

        to: information.To,
        from: information.From,
        body: messageSaludate



    })

    return result

}
module.exports = {
    sendMessageToWhatsApp,
    sendGreetingToWhatsApp
}