'use strict'
const env = process.env.NODE_env || 'development'
const config = require('../config/config')[env]
const request = require('request')


async function getInfoProfile(facebookID) {
    return new Promise((resolve, reject) => {


        request({
            "uri": config.facebook.api_url_profile + facebookID,
            "qs": {
                "access_token": config.facebook.token,
                "fields": 'name'


            }


        }, (err, res, body) => {
            if (err) {
                reject(err)
            } else {
                resolve(JSON.parse(body))
            }
        })
    })

}

module.exports = {
    getInfoProfile
}