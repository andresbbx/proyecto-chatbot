'use strict'
const env = process.env.NODE_ENV || 'development'
const config = require('../config/config')[env]
const request = require('request')
const petitionApi = require('../util/petitionOfApi')
const uri = "pass_thread_control"




async function passControlToSecondaryApp(facebookID){

    let body = {
        "recipient":{"id":facebookID},
        "target_app_id":config.facebook.app_id_secondary,
        "metadata": "main support"
    }

    return petitionApi.sendPetitionApiToFacebook(uri,body)

}


module.exports = {

    passControlToSecondaryApp
}