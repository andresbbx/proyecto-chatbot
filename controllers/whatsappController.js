'use strict'
const ibmService = require('../services/ibm')
const whatsAppModel = require('../models/whastappModel')
//const controlAcessWhatsapp = require('')
const xml = require('xml')
const twilioWhatsapp = require('../services/twilioWhatsapp')




async function pettionsOfWhatsapp(req, res, next) {



    const xmlResponse = xml({ Response: [{ Message: '' }] })
    const toWhatssapp = req.body.To
    const fromWhatsapp = req.body.From
    const messageInsideWhatsapp = req.body.Body


    try {

        if (messageInsideWhatsapp == '') {
            res.set('Content-Type', 'text/xml')


            res.status(204).send(xmlResponse)

        }

        let context = {}

        let userwhatsApp = await whatsAppModel.findOne({ 'usuario.number': fromWhatsapp }).exec()


        if (userwhatsApp) {
            context = userwhatsApp.context

        }

        const responseIbm = await ibmService.sendMessage(messageInsideWhatsapp, context)

        if (userwhatsApp == null) {
            let registeruser = registerUser(fromWhatsapp, responseIbm.context)

        } else {
            userwhatsApp.context = responseIbm.context

            let userupdate = await userwhatsApp.save()
            if (!userupdate) {
                next('usuario no actualizado')
                return
            }


        }


        const information = {
            To: fromWhatsapp,
            From: toWhatssapp
        }

        let sendingResultWhatsapp = null



        if (responseIbm.intents.length == 0) {

            information.message = responseIbm.output.text[0]
            sendingResultWhatsapp = await twilioWhatsapp.sendMessageToWhatsApp(information)


        }


        else if (responseIbm.intents[0].intent === "saludar") {
            information.message = responseIbm.output.text[0]

            sendingResultWhatsapp = await twilioWhatsapp.sendGreetingToWhatsApp(information)

        }
        else if (responseIbm.intents[0].intent === "ubicacion") {
            information.message = responseIbm.output.text[0]


            sendingResultWhatsapp = await twilioWhatsapp.sendMessageToWhatsApp(information)




        }

        else if (responseIbm.intents[0].intent === "servicios") {
            information.message = responseIbm.output.text[0]

            sendingResultWhatsapp = await twilioWhatsapp.sendMessageToWhatsApp(information)
        }

        else if (responseIbm.intents[0].intent === "acciones") {
            information.message = responseIbm.output.text[0]
            sendingResultWhatsapp = await twilioWhatsapp.sendMessageToWhatsApp(information)



        }
        else {

            information.message = responseIbm.output.text[0]
            sendingResultWhatsapp = await twilioWhatsapp.sendMessageToWhatsApp(information)


        }



        if (sendingResultWhatsapp.errorCode == null) {

            res.set('Content-Type', 'text/xml')


            res.status(204).send(xmlResponse)
        } else {

            next(sendingResultWhatsapp.errorMessage)
            return


        }





    } catch (e) {

        if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test') {
            console.log(e)
        }



        next(e)

    }











}


async function registerUser(number, context) {
    return new Promise((resolve, reject) => {

        whatsAppModel.create({ 'usuario.number': number, context: context }, (err, whatsAppModel) => {

            if (err) {
                reject(err)
            } else {
                resolve(whatsAppModel)
            }



        })



    })

}



module.exports = { pettionsOfWhatsapp }