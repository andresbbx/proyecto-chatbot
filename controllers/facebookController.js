'use strict'
const facebookService = require('../services/facebookMessenger')
const ibmService = require('../services/ibm')
const facebookModel = require('../models/facebookModel')
const facebookPlatform = require('../platfforms/facebook.platfform')
const facebookAccesRepository = require('../repository/controlAccessRepository')
const facebookRepository = require('../repository/facebookRepository')
const LIMITEMESSAGES = 20



function validateTokenFacebook(req, res) {

    let mode = req.query['hub.mode']
    let token_verify = req.query['hub.verify_token']
    let challenge = req.query['hub.challenge']

    if (mode == null || token_verify == null || challenge == null) {

        res.status(500).json({ error: 'no se enviaron todos los parametros' })
    }

    else if (mode == 'subscribe' && token_verify == facebookService.token) {
        res.status(200).send(challenge)
    }
    else {
        res.status(500).json({ error: 'no se han enviado el token y los datos' })
    }




}

async function petitionsOfFacebook(req, res, next) {
    try {


        // validateXHeader(req.rawBody,req.headers['x-hub-signature'])

        let entrys = req.body.entry
        let ibmResponse = {}



        if (typeof entrys === 'undefined') {

            res.status(500).send('entry is empty')
            return
        }



        for (let entry of entrys) {




            let userFacebook = entry.messaging[0].sender




            let facebookRegisterModel = await facebookModel.findOne({ "usuario.id_sender": userFacebook.id }).exec()


            let context = {}

            if (facebookRegisterModel) {
                context = facebookRegisterModel.context
            }


            if (entry.messaging[0].referral) {

                ibmResponse = await ibmService.sendMessage("Hola", context)



            }


            else if (entry.messaging[0].postback) {
                if (entry.messaging[0].postback.payload == "Empezar") {
                    ibmResponse = await ibmService.sendMessage("Hola", context)


                }

            }

            else {








                const sendingMessagesCount = await facebookAccesRepository.validateQuantityMessagesSending(userFacebook.id)


                if (sendingMessagesCount.length >= LIMITEMESSAGES) {
                    const menssageUserLimited = " Lo sentimos , me he saturado un poco de mensajes comunicate mañana o envianos un email a" +
                        " contacto@centermas.com y te atenderemos directamente :)"
                    await facebookPlatform.callFacebook(menssageUserLimited, userFacebook.id)
                    res.status(200).send('EVENT_RECEIVED')
                    return
                }



                ibmResponse = await ibmService.sendMessage(entry.messaging[0].message.text, context)

                const registerCountMessage = await facebookAccesRepository.addSendingMessage(userFacebook.id)

                if (facebookRegisterModel == null) {
                    let userRegister = await facebookRepository.registerUser(userFacebook.id, ibmResponse.context)

                } else {
                    facebookRegisterModel.context = ibmResponse.context


                    let facebookupdate = facebookRegisterModel.save()
                    facebookupdate.then((facebookuser) => {
                        if (!facebookuser) {

                            next('Usuario no actualizado')
                            return
                        }

                    })

                }

            }

            let intents = ibmResponse.intents
            let facebookListResponse = []

            if (intents.length == 0) {
                facebookListResponse = await facebookPlatform.callFacebook(ibmResponse.output.text[0], userFacebook.id)
            }
            else {
                facebookListResponse = await facebookPlatform.callFacebook(ibmResponse.output.text[0], userFacebook.id, intents[0].intent)
            }

            for (let facebookResponse of facebookListResponse) {
                if (facebookResponse.error) {
                    next(facebookResponse.error.message)
                }
            }
            res.status(200).send('EVENT_RECEIVED')
        }

    } catch (e) {



        next(e)

    }

}



module.exports = {
    validateTokenFacebook,
    petitionsOfFacebook
}