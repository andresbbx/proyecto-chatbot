'use strict'
const CryptoJs = require('crypto-js')
const env = process.env.NODE_env || 'development'
const config = require('../config/config')[env]

function validateXHeader(req, res, next) {
    if(!("x-hub-signature" in req.headers)){
        throw new Error('Parameter Hubsignature does no exist')
    }

    let rawBody = req.rawBody
    let headerHubSignature = req.headers['x-hub-signature']

    if(headerHubSignature.length==0){
        throw new Error('Parameter Hubsignature is empty')
    }

    let resultTest = "sha1="
    resultTest += CryptoJs.HmacSHA1(rawBody.toString('utf8'),
    config.facebook.app_secret).toString()

    if (resultTest != headerHubSignature) {
        //next(parametro) llama a los  middleware que se encargan de manejar erores 
        next(new Error('Signature hub are different'))
        

    }
    else{
        //sin parametros , pasa al siguiente controlador 
        next()
    }

   

}

module.exports = {

    validateXHeader
}