'use strict'
const facebookService = require('../services/facebookMessenger')
const facebookTool = require('../services/facebookTool')

async function callFacebook(message, userFacebookID, intent=null) {
    let facebookListResponse = []

    facebookListResponse.push(await sendCheckMessageToFacebook(userFacebookID))

    if (!intent) {
        facebookListResponse.push(await sendTextToFacebook(message, userFacebookID))
        facebookListResponse.push(await sendQuestionQuickToFacebook(userFacebookID))
    }
    else if (intent === "saludar") {
        facebookListResponse.push(await sendTextGreetingToFacebook(message, userFacebookID))
    }
    else if (intent === "ubicacion") {
        facebookListResponse.push(await sendTextToFacebook(message, userFacebookID))
        facebookListResponse.push(await sendButtomCallToFacebook(userFacebookID))
    }
    else if (intent === "servicios") {
        facebookListResponse.push(await sendListServicesToFacebook(userFacebookID))
        facebookListResponse.push(await sendTextToFacebook(message, userFacebookID))
    }
    else if (intent === "acciones") {
        facebookListResponse.push(await sendTextToFacebook(message, userFacebookID))
        facebookListResponse.push(await sendQuestionQuickToFacebook(userFacebookID))

    }
    else if (intent === "pasar_control") {
        facebookListResponse.push(await sendTextToFacebook(message, userFacebookID))
        facebookListResponse.push(await passControlApp(userFacebookID))
    }

return facebookListResponse

}

async function sendTextToFacebook(mensaje, userFacebookID) {
    
    return await facebookService.sendTextToFacebook(mensaje, userFacebookID)

}

async function sendTextGreetingToFacebook(mensaje, userFacebookID) {
    return await facebookService.sendTextGreetingToFacebook(mensaje, userFacebookID)
}

async function sendListServicesToFacebook(userFacebookID) {
    return await facebookService.sendListServicesToFacebook(userFacebookID)
}


async function sendQuestionQuickToFacebook(userFacebookID) {
    return await facebookService.sendQuestionQuickToFacebook(userFacebookID)
}

async function sendButtomCallToFacebook(userFacebookID) {
    return await facebookService.sendButtomCallToFacebook(userFacebookID)
}

async function passControlApp(userFacebookID) {
    return await facebookTool.passControlToSecondaryApp(userFacebookID)

}

async function sendCheckMessageToFacebook(userFacebookID) {
    return await facebookService.sendCheckMessageToFacebook(userFacebookID)

}

module.exports = {
callFacebook

}