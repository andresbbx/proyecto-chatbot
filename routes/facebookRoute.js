'use strict'
const express = require('express')
const facebookRoute = express.Router()
const facebookController = require('../controllers/facebookController')
const validateSignature = require('../controllers/validateSignatureHub')

facebookRoute.get('/', facebookController.validateTokenFacebook)
facebookRoute.post('/', validateSignature.validateXHeader,
    facebookController.petitionsOfFacebook)


module.exports = facebookRoute