'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const logsRegister = require('./middleware/log')
const facebookRouter = require('./routes/facebookRoute')
const whatsAppRouter = require('./routes/whatsappRoute')
const app = express()

app.use(bodyParser.json(
    {

        verify(req,res,buf,encoding){
            req.rawBody = buf
          
        }

    }
))

app.use(bodyParser.urlencoded({ extended: false }))
app.use(cors())
app.use('/facebook', facebookRouter)
app.use('/whatsapp', whatsAppRouter)
app.use(logsRegister)

module.exports = app

