'use strict'
const faceebookControlAccessModel = require('../models/ControlAcessModel')
const moment = require('moment')

async function validateQuantityMessagesSending(facebookUserID) {
    const dateToday = moment().startOf('day')

    const registerMenssagesSending = await faceebookControlAccessModel.find({ $and: [{ dateSending: dateToday }, { facebookUserID: facebookUserID }] }).exec()
    return registerMenssagesSending

}


async function addSendingMessage(facebokUserID){


    return new Promise((resolve, reject) => {

        const dateToday = moment().startOf('day')
        faceebookControlAccessModel.create({ dateSending: dateToday, facebookUserID: facebokUserID }, function (err, accessModel) {
            if (err) {
                reject(err)
            } else {
                resolve(accessModel)
            }
        })

    })


}

module.exports = {

    validateQuantityMessagesSending,
    addSendingMessage

}