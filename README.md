# ChatBot

Bienvenidos al repositorio, este proyecto consiste en un API desarrollada en NodeJS la
cual se comunica con dos servicios externos Facebook Messenger Platform, y IBM Watson
logrando construir un bot que acutlmente se encuentra en [produccion](https://www.facebook.com/mascontactcenter/)

### Metodologia 

Utilizamos la metodologia [ChatBot Desing Canvas](https://pochocosta.com/podcast/chatbot-design-canvas/), es una metodologia basada en el modelo canvas, el cual facilita la recoleccion de requerimientos y el proceso del desarrollo del Bot enfocandose siempre en la interaccion con el cliente

![alt text](https://i.ibb.co/9wLVpgg/pasted-image-0.png) 

### Requerimientos

Es necesario tener instalado las siguientes herramientas

* Nodejs >= v10.15.0
* Mongo  >=  v4.0.13

### ¿Cómo empezar con este repositorio?

Para empezar con una copia de este repositorio es suficiente con hacer un **fork** y ya tendrá su propia copia del repositorio en su cuenta personal. Aunque si gusta puede clonar el repositorio usando el comando *git clone*.

`git clone https://gitlab.com/andresbbx/proyecto-chatbot.git`


#### Configuracion de credenciales

Para ejecutar este proyecto se deben configurar dos servicios de terceros ,  Facebook Messenger Platform el cual sera la plataforma en la cual se desplegara nuestro Bot e IBM Watson el cual se encargara de procesar los mensajes y proporcionar las respuestas,
lo primero que se debe de hacer es renombrar el archivo  *env-example* a *.env*  en linux se puede ejecutar el comando `mv .env-example .env` este archivo contiene todas las variables de entorno necesarias para ejecutar el proyecto

**Para la API de Facebook Messenger Platform  se necesitan las siguientes credenciales :**

*  Token de acceso a Facebook
*  Clave secreta de la app

**Para la API de IBM se necesitan las siguientes credenciales**

*  API key o clave de acceso a la API
*  URL de acceso a IBM Watson
*  El Workspace ID


### Obtener Credenciales de  Facebook Messenger Platform

En le siguiente video se muestra como obtener **El token de acceso** y **la clave secreta de la app** (en ese orden) se necesita crear una pagina de facebook , y una app en la pagina [developers facebook ](https://developers.facebook.com/) 

[![](http://img.youtube.com/vi/jMjB3moSOz0/0.jpg)](http://www.youtube.com/watch?v=jMjB3moSOz0 "Credenciales de Facebook")

Despues de obtener las credenciales en el archivo renombrado **.env** se modifican los siguientes  campos 

`TOKEN_FACEBOOK_MESSENGER = token obtenido `

`APP_SECRET_FACEBOOK = clave secreta obtenida`

### Obtener Credenciales de  IBM Watson

Para poner a funcionar  IBM Watson, encargado de procesar los textos, deberia de realizarse un entrenamiento previo, por el cuial se le indica por medio de ejemplos que debe responder a ciertas preguntas, este entrenamiento ya lo hicimos y lo incluimos dentro del proyecto para que 
solo lo importes a la plataforma , este entrenamiento se encuentra en el  archivo llamado **skill-chat-centermas.json** para saber somo importar este archivo y obtener las credenciales revisa el siguiente video

[![](http://img.youtube.com/vi/t47Tv3bVAm4/0.jpg)](http://www.youtube.com/watch?v=t47Tv3bVAm4 "Credenciales de IBM Watson")

En el anterior video las primeras credenciales que se obtienen son **API key o clave de acceso a la API** y **URL de acceso a IBM Watson** las cuales se encuentran en el archivo que se descarga
el Workspace id se encuentra en la seccion *Skill Details* en el campo *Legacy V1 Workspace URL*  https://api.us-south.assistant.watson.cloud.ibm.com/instances/*****/v1/workspaces/**workspaceID**/message

Despues de obtener las credenciales en el archivo renombrado **.env** se modifican los siguientes campos 

`IBM_IAM_APIKEY =  API key del archivo descargado`

`IBM_URL = url de ibm del archivo descargado`

`IBM_WORKSPACE_ID = el id del Wrokspace obtenido de la url `

### Configurar mongoDB

El poryecto utiliza mongo para realizar registros de cualquier error que ocurra en la aplicacion, por eso es necesario actualizar en el archivo **.env** la siguiente variable

`MONGO_URL = localhost`

`MONGO_URL_TEST = localhost`

En localhost debe de ir la ip de su servidor, si este tiene usuario y password les recomiendo leer la [documentacion oficial](https://mongoosejs.com/docs/) de la libreria para configurarlo


### Ejecutar el  proyecto en  desarrollo

Este proyecto se ejecutara en desarrollo, por eso en el archivo **.env** en el campo `NODE_ENV = development` se debe poner *development* si desea publicar el bot en facebook para que funcione para todo publico [diriganse a la documentacion oficial ](https://developers.facebook.com/docs/messenger-platform/submission-process)

**Instalar dependencias**

Lo primero a realizar es instalar las dependencias necesarias, para eso nos ubicamos en el directorio raiz del proyecto y  ejecutamos el comando 

`npm install`

**Ejecutar Test**

Para asegurarnos que todo esta funcionando correctamente se pueden ejecutar los test utilizando el comando 

`npm run test`

**Ejecutar proyecto**

Para ejecutar el proyecto en modo desarrollo ejecutamos el comando

`npm run dev`

![alt text](https://i.ibb.co/512W0fV/Screenshot-430.png) 

### Utilizar ngrok

Es necesario para realizar las pruebas con Facebook Platform Messenger que la url tenga acceso publico, para eso utilizaremos este proxy, se dirigen a la pagina https://ngrok.com/
descargan ewl proyecto , se ubican en la carpeta y ejecutan ngrok

`./ngrok http 300`

### Configurar WebHook de facebook

Ahora debemos configurar el WebHook , es la url a donde facebook nos enviara todos los mensajes que le escriban a nuestra pagina que utilizaremos como bot
por eso era necesario el proxy, ya que esta url debe ser publica, se dirigen a la pagina de [facebook developers](https://developers.facebook.com/) se dirigen a la configuracion de la app creada anteriormente en la seccion de *webhooks* 
editan la suscripcion ,digitan la url con https que ngrok les asigno (el endpoint del proyecto encargado de validar el token es /webhook) y se digita el token de facebook obtenido anteriormente dando en el boton validar , como se 
muestra en la imagen 

![alt text](https://i.ibb.co/MGJ5cT6/Screenshot-427.png) 


### Desplegar en Google App Engine

Para desplegar en Google App Engine se necesita la configuracion del archivo **app.yaml** el cual ya se encuentra dentro del proyecto, se deben de dirigir a [Google App Engine](https://cloud.google.com/appengine) y registrar una cuenta, inicalmente tienen unos creditos gratuitos, ya aqui se deben dirigir a la consola y digitar los siguientes comandos 

**Clonar repositorio**

`git clone https://gitlab.com/Quarners/botcentermas.git`

**Luego deben de crear el archivo '.env'**

`touch .env`

y aqui ingresan las variables de entorno que configuraron en el proyecto anteriormente

**y luego ejecutan**

`gcloud app deploy`

Esto les retorna un endpoint el cual es la url para acceder a su proyecto desplegado (se debe de actualizar de nuevoi el Webhook de facebook por esta nueva url agregando */webhook*)

### Arquitectura de Despliegue final

![alt text](https://i.ibb.co/R0YZjg8/Untitled-Diagram-16-1.jpg) 



# Contribuidores

Este es un proyecto desarrollado por 

- [Jairo Castañeda](https://gitlab.com/andresbbx)
- [Emanuel Martínez](https://gitlab.com/emartinezpinzon)
- [Jeison Rangel](https://gitlab.com/JeisonRangel)

---
⌨️ con ❤  por  [Quarners](https:/quarners.comd) 😊

