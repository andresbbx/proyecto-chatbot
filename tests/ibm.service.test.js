'use strict'
const ibmService = require('../services/ibm')



jest.mock('watson-developer-cloud/assistant/v1', () => {
    //Simula  la peticion a Ibm  para no realizar test que consuman recursos 
    const message = jest.fn((options, callback) => {
        const response = {}
        callback(null, response)
    })


    return jest.fn().mockImplementation(() => {
        return { message: message }
        
    })
})


describe('service ibm', () => {



    it('parameter null', async() => {
       
       
        await expect(ibmService.sendMessage(null, null)).rejects.toThrow()
        

    })

})