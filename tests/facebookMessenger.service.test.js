'use strict'
const request = require('request')
const facebookService = require('../services/facebookMessenger')
const componentFacebook = require('../util/componentFacebook')

jest.mock('request')

jest.mock('../services/facebookInfoProfile',()=>{

})



describe('service facebook, sendtext',()=>{

request.mockImplementation((options,callback)=>{
    callback(null,{},{})
    
})

    it('send text  with null',async ()=>{

     await expect(facebookService.sendTextToFacebook(null,null)).rejects.toThrow()
      
    })



    it('send text  with string empty',async()=>{
        await expect(facebookService.sendTextToFacebook('','')).rejects.toThrow()
    })

})

describe('service facebook  list services',()=>{

    jest.mock('../util/petitionOfApi.js',()=>{
                
        const sendPetitionApiToFacebook = jest.fn((uri,body)=>{
        return body

        })


        return jest.fn().mockImplementation(()=>{
            return { sendPetitionApiToFacebook}
        })


    })




    request.mockImplementation((options,callback)=>{
        callback(null,{},{})
        
    })
    
        it('send text service with null',async ()=>{
    
         await expect(facebookService.sendListServicesToFacebook(null,null)).rejects.toThrow()
          
        })
    
    
    
        it('send services service with string empty',async()=>{
            await expect(facebookService.sendListServicesToFacebook('')).rejects.toThrow()
        })



        it('send list sservices',async()=>{

            
           


           const result = await facebookService.sendListServicesToFacebook('23874564578')
           expect(result.message).toEqual(componentFacebook('listServices').message) 

        })

        it('send buttom call',async()=>{
            const idFacebook = '233434343'
            const result = await facebookService.sendButtomCallToFacebook(idFacebook)
            expect(result.message).toEqual(componentFacebook('buttomCall').message)

        })


        it('send question quick',async()=>{
            const idFacebook = '2323232'
            const result = await facebookService.sendQuestionQuickToFacebook(idFacebook)
            expect(result.message).toEqual(componentFacebook('questionQuick').message)


        })
    
    })


describe('service check message ',()=>{



    it('send facebook id empty',async()=>{

        await expect(facebookService.sendCheckMessageToFacebook('')).rejects.toThrow()

    })
})
