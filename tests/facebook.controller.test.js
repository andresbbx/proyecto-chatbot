'use strict'
const moment = require('moment')
const facebookController = require('../controllers/facebookController')
const ibmService = require('../services/ibm')
const facebookModel = require('../models/facebookModel')
const mockingoose = require('mockingoose').default
const facebookService = require('../services/facebookMessenger')
const controlAccesModel = require('../models/ControlAcessModel')
const facebookTool = require('../services/facebookTool')




jest.mock('../services/facebookTool')

jest.mock('../services/facebookMessenger', () => {
    const sendCheckMessageToFacebook = jest.fn(async text => {

        return new Promise((resolve, reject) => {
            resolve(text)

        })
    })

    const sendTextToFacebook = jest.fn(async text => {

        return new Promise((resolve, reject) => {
            const result = {
                recipient_id: '12'
            }
            resolve(result)
        }


        )


    })

    return {
        sendCheckMessageToFacebook: sendCheckMessageToFacebook,
        sendTextToFacebook: sendTextToFacebook
    }

})
jest.mock('../services/ibm', () => {
    const sendMessage = jest.fn(async text => {

        return new Promise((resolve, reject) => {
            const result = {
                intents: [],
                output: {
                    text: ['mensaje prueba']
                }
            }

            resolve(result)

        })
    })

    return {
        sendMessage: sendMessage
    }

})


afterEach(() => {
    facebookService.sendTextToFacebook.mockClear()
})

beforeEach(() => {

    jest.clearAllMocks();
})


const jsonresponse = function (serializable) {
    this.error = serializable
}

const statusResponse = function (responseStatus) {
    this.codeStatus = responseStatus
    return this
}

const send = function (error) {
    this.errorMessage = error
}

const response = {
    status: statusResponse,
    json: jsonresponse,
    send: send

}


function cleanData(){
response.error = {error:""}
response.codeStatus = 0
response.errorMessage = ""

}

describe('validate token facebook', () => {


    it('validate parameter hubmode   null', () => {

        const req = {
            query: {
                'hub.verify_token': '',
                'hub.challenge': ''

            }

        }

     
        cleanData()
        facebookController.validateTokenFacebook(req, response)

        expect(response.codeStatus).toEqual(500)
        expect(response.error).toEqual( {error:'no se enviaron todos los parametros'} )



    })

    describe('petitions of facebook', () => {
        const facebookPlataforma = {

            usuario: {
                id_sender: '287176723434',
                contex: {}
            }

        }

        const date = new Date
        let dateToday = moment(date.toString(), moment.ISO_8601)
        dateToday = moment().startOf('day')
        dateToday = moment(dateToday).endOf('day').toDate()

        const objetControl = {
            facebokUserID: '287176723434',
            dateSending: dateToday
        }

        let controlAccesResult = [

        ]



        mockingoose(facebookModel).toReturn(facebookPlataforma, 'findOne')
        mockingoose(facebookModel).toReturn(facebookPlataforma, 'save')
        mockingoose(controlAccesModel).toReturn(controlAccesResult, 'find')



        const body = {

            "entry": [

                {

                    "messaging": [
                        {

                            "sender": {
                                "id": "287176723434"
                            },
                            "message": {
                                "text": "hola que tal"
                            }
                        }


                    ]

                },
            ]
        }


        it('entry empty', async () => {
           

            const req = {
                body: {}
            }

            cleanData()
            const resultado = await facebookController.petitionsOfFacebook(req, response, () => { })
            expect(response.codeStatus).toEqual(500)
            expect(response.errorMessage).toEqual('entry is empty')




        })


        it('entrys with information', async () => {


            cleanData()

            const req = {
                body: body
            }



            const resultado = await facebookController.petitionsOfFacebook(req, response, () => { })

            expect(ibmService.sendMessage.mock.calls.length).toBe(1)


        })

        it('send text facebook ', async () => {



            cleanData()

            const req = {
                body: body
            }



            const resultado = await facebookController.petitionsOfFacebook(req, response, () => { })

            expect(facebookService.sendTextToFacebook.mock.calls.length).toBe(1)


        })


        it('limit ibm send', async () => {


            for (let i = 21; i > 0; i--) {
                controlAccesResult.push(objetControl)
            }


            cleanData()

            const req = {
                body: body
            }


            const resultado = await facebookController.petitionsOfFacebook(req, response, () => { })
            expect(facebookService.sendTextToFacebook.call.length).toBe(1)
        })



        it('pass control of bot to person',async()=>{
            const result = {
                intents: ['pasar_control'],
                output: {
                    text: ['ya te atendera un personal del area de soporte']
                }
            }
            ibmService.sendMessage.mockResolvedValue(result)

            cleanData()
            

            const req = {
                body: body
            }
            const resultfacebookTool = {
                recipient_id: '12'
            }
            
            facebookTool.passControlToSecondaryApp.mockResolvedValue(resultfacebookTool)

            const resultado = await facebookController.petitionsOfFacebook(req, response, () => { })
            expect(facebookTool.passControlToSecondaryApp.call.length).toBe(1)
            


        })


    })




})