'use strict'
const whatsappController = require('../controllers/whatsappController')
const whatsappModel = require('../models/whastappModel')
const mockingoose = require('mockingoose').default

const next = jest.fn()



jest.mock('../services/ibm', () => {
    const sendMessage = jest.fn(async text => {

        return new Promise((resolve, reject) => {
            const result = {
                intents: [],
                output: {
                    text: ['mensaje prueba']
                },
                context: {}
            }

            resolve(result)

        })
    })

    return {
        sendMessage: sendMessage
    }

})


const {sendMessageToWhatsApp} = require('../services/twilioWhatsapp')


jest.mock('../services/twilioWhatsapp', () => {
    
    
    
        const sendMessageToWhatsApp = jest.fn()

        return {sendMessageToWhatsApp}
    

})






beforeEach(() => {

    jest.clearAllMocks();
})




describe('send message whatsapp' ,()=>{
   
    const whatsappplatform = {

        usuario: {
           number: 'whatsapp:+573052995595'
        },
        context:{}

    }

    mockingoose(whatsappModel).toReturn(whatsappplatform, 'findOne')
    


    it('send message with all parameter',async()=>{
        const resultado = {
            errorCode: null
         }
         sendMessageToWhatsApp.mockResolvedValue(resultado)

        

        const req = {
            body: {
                To:'783748734834',
                From:'whatsapp:+573052995595',
                Body:'Hola'
                 }
        }


        const statusResponse = function (responseStatus) {
            this.codeStatus = responseStatus
            return this
        }

        const send = function (message) {
            this.message = message
        }

        const response = {
            status: statusResponse,
            send: send,
            set:(atributte)=>{}

        }


        const result = await whatsappController.pettionsOfWhatsapp(req,response,next)
      
        expect(response.codeStatus).toEqual(204)
        
        


    })


    it('send message with parameter in body empty',async()=>{

        

       

        const req = {
            body: {
                To:'783748734834',
                From:'whatsapp:+573052995595',
                Body:''
                 }
        }


        const statusResponse = function (responseStatus) {
            this.codeStatus = responseStatus
            return this
        }

        const send = function (message) {
            this.message = message
        }

        const response = {
            status: statusResponse,
            send: send,
            set:(atributte)=>{}

        }


        const result = await whatsappController.pettionsOfWhatsapp(req,response,next)
      
        expect(response.codeStatus).toEqual(204)
        
        


    })


    


})


describe('generate errors',()=>{
  

    const whatsappplatform = {

        usuario: {
           number: 'whatsapp:+573052995595'
        },
        context:{}

    }

    mockingoose(whatsappModel).toReturn(whatsappplatform, 'findOne')







it('error whastapp',async()=>{
    const resultado = {
        errorCode: 300,
        errorMessage:"error"
     }
     sendMessageToWhatsApp.mockResolvedValue(resultado)
  


    const req = {
        body: {
            To:'783748734834',
            From:'whatsapp:+573052995595',
            Body:''
             }
    }


    const statusResponse = function (responseStatus) {
        this.codeStatus = responseStatus
        return this
    }

    const send = function (message) {
        this.message = message
    }

    const response = {
        status: statusResponse,
        send: send,
        set:(atributte)=>{}

    }


    const result = await whatsappController.pettionsOfWhatsapp(req,response,next)
  
    expect(response.codeStatus).toEqual(204)
    




})


})