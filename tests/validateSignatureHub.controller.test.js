'use strict'
const validateSignatureHub = require('../controllers/validateSignatureHub')


describe('parameters for  xhub', () => {


    it('parameter empty', () => {

        const req = {
            body: {},
            headers: {
                'x-hub-signature': ''

            }
        }

        expect(() => {

            validateSignatureHub.validateXHeader(req, {}, () => {})

        }).toThrow();

    })

    it('the hub signature does not exist',()=>{

        const req = {
            body: {},
            headers: {
                

            }
        }

        expect(()=>{

            validateSignatureHub.validateXHeader(req, {}, () => {})

        }).toThrow()

    })




})