'use strict'
const fs = require('fs')


async function readJsonFile(ubicationFile) {
    return new Promise((resolve, reject) => {
        fs.readFile(ubicationFile, 'utf8', (err, data) => {
            if (err) {
                reject(err)
            }

            resolve(data)

        })


    })
}

async function readComponets() {
    let data = await readJsonFile('conversationComponentsFacebook.json')
    return JSON.parse(data)

}

async function getComponentFacebook(nameComponent) {
    let componets = await readComponets()
    if (nameComponent === 'listServices') {
        return componets.listServices
    }
    else if (nameComponent === 'questionQuick') {
        return componets.questionQuick

    }

    return componets.buttomCall

}



module.exports = getComponentFacebook