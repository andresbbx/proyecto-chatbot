const env = process.env.NODE_ENV || 'development'
const config = require('../config/config')[env]
const request = require('request')


async function sendPetitionApiToFacebook( uri, body) {
    let urlFinal = config.facebook.api_url

    if (uri ==! null) {
        urlFinal += uri
    }
  

    return new Promise((resolve, reject) => {

        request({
            "url": config.facebook.api_url+uri,
            "qs": {
                "access_token": config.facebook.token
            },
            "method": "POST",
            "json": body

        }, (err, res, bodyFacebook) => {
            if (err || bodyFacebook.error) {
                reject(err)
            } else {
                resolve(bodyFacebook)
            }
        })



    })


}


module.exports = {
    sendPetitionApiToFacebook

}