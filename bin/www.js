'use strict'
const mongoose = require('mongoose')
const env = process.env.NODE_ENV|| 'development'
const config = require('../config/config')[env]
const app = require('../app')
const chalk = require('chalk')

function initDB(){
    let options =  { useNewUrlParser: true }
    
    mongoose.connect(config.mongodb,options)

    let resultadoConecction = mongoose.connection
    resultadoConecction.on('error', (error) => {
        console.log('error ', error)
    })
    resultadoConecction.once('open', () => {
        console.log(chalk.bold.green('conexion exitosa'))
        //console.log(config.mongodb)

    })

}


function upServer(){
    app.listen(process.env.PORT,()=>{
        console.log(chalk.bold.green('EJECUNTADOSE EN EL PUERTO :'))
        console.log(chalk.bgWhite.red
            (''+process.env.PORT))

    })
}

initDB()
upServer()