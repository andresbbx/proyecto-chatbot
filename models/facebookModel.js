'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const facebookSchema = new Schema({

    context: {
        
    },
    usuario:{
        id_sender:{
            type:String
        },
        name:{
            type:String
        },
        dateMessages:[Date]

    }

})

module.exports = mongoose.model('facebookPlatform', facebookSchema)