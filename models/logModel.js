'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema


const logSchema = new Schema({

    error: {
        type: String
    }
    ,
    dateOfProblem :{
        type:Date,
        default:Date.now
    }
})

module.exports = mongoose.model('logModel', logSchema)