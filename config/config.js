'use strict'
require('dotenv').config()

module.exports = {


    development: {

        mongodb: process.env.MONGO_URL,
        ibm: {
            iam_apikey: process.env.IBM_IAM_APIKEY,
            url: process.env.IBM_URL,
            version: process.env.IBM_VERSION,
            workspaceid: process.env.IBM_WORKSPACE_ID

        },
        facebook: {
            token: process.env.TOKEN_FACEBOOK_MESSENGER,
            api_url: process.env.URL_FACEBOOK_API,
            api_url_profile: process.env.URL_FACEBOOK_API_PROFILE,
            app_id_secondary: process.env.APP_ID_HADNOVER_INBOX,
            app_secret : process.env.APP_SECRET_FACEBOOK
        },

        twilio:{

            accountSid: process.env.ACCOUNTSID_TWILIO,
            authToken:process.env.AUTHTOKEN_TIWLIO
        }

    },
    production: {

        mongodb: process.env.MONGO_URL,
        ibm: {
            iam_apikey: process.env.IBM_IAM_APIKEY,
            url: process.env.IBM_URL,
            version: process.env.IBM_VERSION,
            workspaceid: process.env.IBM_WORKSPACE_ID,
            app_id_secondary: process.env.APP_ID_HADNOVER_INBOX

        },
        facebook: {
            token: process.env.TOKEN_FACEBOOK_MESSENGER,
            api_url: process.env.URL_FACEBOOK_API,
            api_url_profile: process.env.URL_FACEBOOK_API_PROFILE,
            app_id_secondary: process.env.APP_ID_HADNOVER_INBOX,
            app_secret : process.env.APP_SECRET_FACEBOOK
        },
        
        twilio:{

            accountSid: process.env.ACCOUNTSID_TWILIO,
            authToken:process.env.AUTHTOKEN_TIWLIO
        }

    },

    test: {

        mongodb: process.env.MONGO_URL_TEST,
        ibm: {
            iam_apikey: '',
            url: '',
            version: '',
            workspaceid: ''

        },
        facebook: {
            token:'',
            api_url: '',
            api_url_profile: ''
        }

    },
    node: {

        mongodb: process.env.MONGO_URL_TEST,
        ibm: {
            iam_apikey: '',
            url: '',
            version: '',
            workspaceid: ''

        },
        facebook: {
            token:'',
            api_url: '',
            api_url_profile: ''
        }

    },
    twilio:{

        accountSid: process.env.ACCOUNTSID_TWILIO,
        authToken:process.env.AUTHTOKEN_TIWLIO
    }
}